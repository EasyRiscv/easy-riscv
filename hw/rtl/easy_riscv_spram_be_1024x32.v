//-------------------------------------- 
// easy-riscv single port byte write enable
//--------------------------------------

module EASY_RISCV_SPRAM_BE #(
    parameter   DAT_W   = 32   
    ,          ADDR_W   = 10            // 1024 x 32-bit = 4KB  
    ,           COL_W   = 8             // 1x Byte  
    ,           ENA_W   = DAT_W/COL_W   // 1x Byte  
)
//--------------------------------------
(
//--------------------------------------
    input                       clk 
,   input                       ce      // active high 
,   input       [ENA_W-1:0]     we      // 1: byte enable write, all 0: read 
,   input      [ADDR_W-1:0]     addr       
,   input       [DAT_W-1:0]     din 
//--------------------------------------
,   output reg  [DAT_W-1:0]     dout     
//--------------------------------------
);
//--------------------------------------
reg     [DAT_W-1:0]     mem[2**ADDR_W-1:0]   ;
//--------------------------------------
integer byte_idx;
always @(posedge clk) begin
    if (ce == 1'b1) begin
        for (byte_idx=0;byte_idx<ENA_W;byte_idx=byte_idx+1) begin
            if (we[byte_idx] == 1'b1) begin
                mem[addr][byte_idx*COL_W +:COL_W]   <= din[byte_idx*COL_W +:COL_W]  ;
            end
        end
        if (we == {ENA_W{1'b0}}) begin
            dout    <= mem[addr]    ; 
        end
    end
end

//--------------------------------------
endmodule
