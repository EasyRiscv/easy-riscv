// DESCRIPTION: Verilator: Verilog example module
//
// This file ONLY is placed under the Creative Commons Public Domain, for
// any use, without warranty, 2017 by Wilson Snyder.
// SPDX-License-Identifier: CC0-1.0
//======================================================================

// For std::unique_ptr
#include <memory>

// Include common routines
#include <verilated.h>

// Include model header, generated from Verilating "top.v"
#include "Vtop.h"

// Legacy function required only so linking works on Cygwin and MSVC++
double sc_time_stamp() { return 0; }

int main(int argc, char** argv, char** env) {
    // This is a more complicated example, please also see the simpler examples/make_hello_c.

    int clk_period=10;
    int cyc_cnt=0;
    
    long long sim_time_ms=10;
    long long sim_time_ns;
    sim_time_ns = sim_time_ms*1000000;

    // Prevent unused variable warnings
    if (false && argc && argv && env) {}

    // Create logs/ directory in case we have traces to put under it
    Verilated::mkdir("logs");

    // Construct a VerilatedContext to hold simulation time, etc.
    // Multiple modulecomplie later below with Vtop) may share the same
    // context to share time, or modules may have different contexts if
    // they should be independent from each other.

    // Using unique_ptr is similar to
    // "VerilatedContext* contextp = new VerilatedContext" then deleting at end.
    const std::unique_ptr<VerilatedContext> contextp{new VerilatedContext};

    // Set debug level, 0 is off, 9 is highest presently used
    // May be overridden by commandArgs argument parsing
    contextp->debug(9);
    
    // Randomization reset policy
    // May be overridden by commandArgs argument parsing
    contextp->randReset(2);

    // Verilator must compute traced signals
    contextp->traceEverOn(true);

    // Pass arguments so Verilated code can see them, e.g. $value$plusargs
    // This needs to be called before you create any model
    contextp->commandArgs(argc, argv);

    // Construct the Verilated model, from Vtop.h generated from Verilating "top.v".
    // Using unique_ptr is similar to "Vtop* top = new Vtop" then deleting at end.
    // "TOP" will be the hierarchical name of the module.
    const std::unique_ptr<Vtop> top{new Vtop{contextp.get(), "TOP"}};

    // Set Vtop's input signals
    top->i_clk      = 0     ;
    top->i_rst_n    = 0x0   ;
    top->i_start_pc = 0x0   ;
    top->i_excp_mask_config = 0x0   ;
    
    cyc_cnt = 20;
    for (int i=0;i<(cyc_cnt*2);i++) {
        contextp->timeInc(clk_period/2);        // 1 timeprecision period passes...
        top->i_clk = !top->i_clk;
        top->eval();
        // VL_PRINTF("[%" VL_PRI64 "d] clk=%x rstl=%x i_start_up=%x i=%d \n",
        //           contextp->time(), top->i_clk, top->i_rst_n, top->i_start_up, i);
    }    
    top->i_rst_n    = 0x1   ;

    // Simulate until $finish
    while(!contextp->gotFinish()) {
        contextp->timeInc(clk_period/2);  // 1 timeprecision period passes...
        top->i_clk = !top->i_clk;
        top->eval();
        if ((contextp->time() > sim_time_ns) || (top->o_exceptions > 0x0) || (top->o_sim_end == 0x1)) {
            break;
        }
    }
    
    // Final model cleanup
    top->final();
    
    // Coverage analysis (calling write only after the test is known to pass)
#if VM_COVERAGE
    Verilated::mkdir("logs");
    contextp->coveragep()->write("logs/coverage.dat");
#endif

    // Return good completion status
    // Don't use exit() or destructor won't get called
    return 0;
}
