`ifndef __EASY_RISCV_DEF__
`define __EASY_RISCV_DEF__

`define RISCV_LUI         6'd0
`define RISCV_AUIPC       6'd1
`define RISCV_JAL         6'd2
`define RISCV_JALR        6'd3
`define RISCV_BEQ         6'd4
`define RISCV_BNE         6'd5
`define RISCV_BLT         6'd6
`define RISCV_BGE         6'd7
`define RISCV_BLTU        6'd8
`define RISCV_BGEU        6'd9
`define RISCV_LB          6'd10
`define RISCV_LH          6'd11
`define RISCV_LW          6'd12
`define RISCV_LBU         6'd13
`define RISCV_LHU         6'd14
`define RISCV_SB          6'd15
`define RISCV_SH          6'd16
`define RISCV_SW          6'd17
`define RISCV_ADDI        6'd18
`define RISCV_SLTI        6'd19
`define RISCV_SLTIU       6'd20
`define RISCV_XORI        6'd21
`define RISCV_ORI         6'd22
`define RISCV_ANDI        6'd23
`define RISCV_SLLI        6'd24
`define RISCV_SRLI        6'd25
`define RISCV_SRAI        6'd26
`define RISCV_ADD         6'd27
`define RISCV_SUB         6'd28
`define RISCV_SLL         6'd29
`define RISCV_SLT         6'd30
`define RISCV_SLTU        6'd31
`define RISCV_XOR         6'd32
`define RISCV_SRL         6'd33
`define RISCV_SRA         6'd34
`define RISCV_OR          6'd35
`define RISCV_AND         6'd36
`define RISCV_FENCE       6'd37
`define RISCV_FENCEI      6'd38
`define RISCV_ECALL       6'd39
`define RISCV_EBREA       6'd40
`define RISCV_CSRRW       6'd41
`define RISCV_CSRRS       6'd42
`define RISCV_CSRRC       6'd43
`define RISCV_CSRRWI      6'd44
`define RISCV_CSRRSI      6'd45
`define RISCV_CSRRCI      6'd46
`define RISCV_WFI         6'd47
`define RISCV_ILLEG_INSTR 6'd48

`endif
