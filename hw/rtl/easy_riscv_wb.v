//-------------------------------------- 
// easy-riscv writeback (commit)
//--------------------------------------

module EASY_RISCV_WB #(
    parameter   DAT_W   = 32   
    ,           ENA_W   = DAT_W/8           
    ,       GPR_IDX_W   = 5             // GPR_IDX   
)
//--------------------------------------
(
//--------------------------------------
    input                       i_clk 
,   input                       i_rst_n 
,   input       [5      :0]     i_what_is_instr_e1  
,   input       [ENA_W-1:0]     i_rf_rd_be_e1                   // 1: byte read data vld, include i_instr_vld_e1
,   input   [GPR_IDX_W-1:0]     i_rf_rd_idx_e1                  // target register index                    
,   input       [DAT_W-1:0]     i_rf_rd_dat_e1 
//--------------------------------------
,   output  [GPR_IDX_W-1:0]     o_rf_rd_idx_c0                  // target register index 
,   output                      o_rf_rd_vld_c0                  // target register write valid  
,   output      [DAT_W-1:0]     o_rf_rd_dat_c0                  // target register value
//--------------------------------------
);
//--------------------------------------
reg     [DAT_W-1:0]     load_data_c0    ;

//--------------------------------------
always @(*) begin
    load_data_c0    = {DAT_W{1'b0}} ;
    // Need Fill !!!
end

//--------------------------------------
assign o_rf_rd_vld_c0   = |i_rf_rd_be_e1    ;
assign o_rf_rd_idx_c0   = i_rf_rd_idx_e1    ;
assign o_rf_rd_dat_c0   = load_data_c0      ;

//--------------------------------------
endmodule
