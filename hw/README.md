# easy-riscv hw env

#### 目录架构
```
hw                                      #	硬件代码目录
├── rtl                                 #	RTL 代码和 filelist (使用者需要补全 RTL 并通过 verify 测试)
│   ├── easy_riscv.f                    # 	输入给 Verilator 的 filelist        
│   ├── easy_riscv_def.v                #	包含目前支持的 RV32I 指令宏定义 (Golden)
│   ├── easy_riscv_idu.v                #	decode and issue
│   ├── easy_riscv_iex_lsu.v            #	iex and lsu
│   ├── easy_riscv_ifu.v                #	ifu
│   ├── easy_riscv_spram_be_1024x32.v   #	imem and dmem 写当拍写入, 读下拍出数
│   ├── easy_riscv_top.v                #	RTL 顶层
│   └── easy_riscv_wb.v                 #	write back to commit
└── verify                              #	验证目录
    ├── top.v                           #	Verilog Testbench (包含 EASY_RISCV_TOP 例化)
    ├── sim_main.cpp                    #	Verilator Testbench (包含 Verilog Testbench 例化)
    ├── Makefile                        #	编译脚本 (支持 make cmp 编译, make run 运行, make wave 开波形)
    ├── run.sh                          #	回归脚本 (支持3个选项 simple-asm, simple-c, isa, 分别 run 不同测试)
    ├── logs                            #	临时目录包含 easy_riscv.vcd 波形
    ├── testcase.dump                   # 	临时文件当前用例反汇编
    ├── inst.bin.str                    #	临时文件当前用例指令段加载文件
    ├── data.bin.str                    #	临时文件当前用例数据段加载文件
    └── run_results                     #	临时目录包含用例 log
```

#### Testbench 例化
- 整体 Testbench 例化层次如下图

![输入图片说明](https://images.gitee.com/uploads/images/2021/0809/200255_2c18f544_8995020.png "tb.png")

- top.v 包含 start_pc 控制, 与 sw 配合实现打印字符和判断用例是否结束 (若更改 MEM SIZE 需要注意), 指令段和数据段加载
- sim_main.cpp 是 Verilator 总体控制, 负责时钟复位生成和仿真结束判断

#### 用例 PASSED 打印

- simple-asm PASSED

![输入图片说明](https://images.gitee.com/uploads/images/2021/0809/191637_19f36bb5_8995020.png "hellp.png")

- simple-c PASSED

![输入图片说明](https://images.gitee.com/uploads/images/2021/0809/191656_c097b388_8995020.png "hello_c.png")

- isa PASSED

![输入图片说明](https://images.gitee.com/uploads/images/2021/0809/191711_65187bc6_8995020.png "hello_isa.png")


