
### EASY-RISCV

### 介绍
- easy-riscv-v1.0 是基于 Verilog 为 CPU 初学者或爱好者搭建的一个 3-stage pipeline, single issue, in-order 的 RISC-V 虚拟项目

### 主要特性
- 支持RV32I指令集 (除 fence，ecall，ebreak 指令) 
- 支持汇编, C, ISA指令集测试
- IMEM 2KB, DMEM 4KB 
- 不支持非对齐取指和访问

### 使用方法
- 在 hw/rtl 目录完成流水线代码填空
- 在 hw/verify 完成代码编译, 调试通过目前支持的测试 (simple-asm, simple-c, isa)
- 上述基本项目完成后, 也欢迎添加分支预测, 扩展流水线, 修改完善验证环境等...

### 环境需求
```
- OS  系统: Linux                    #    Ubuntu 20.04 LTS (其他发行版需安装依赖)
- RTL 编译: verilator v4.204         #    https://gitee.com/EasyRiscv/verilator.git
- vcd 波形: gtkwave                  #    sudo apt-get install gtkwave
- gnu 编译: riscv-gnu-tool           #    https://gitee.com/EasyRiscv/riscv-gnu-tool.git
```
- 因 Ubuntu 20.04 不用安装过多依赖, 所以强烈推荐使用
- win10 搭建 WSL2 + MobaXterm 流程可参考 https://blog.csdn.net/robbie1121/article/details/119762452
```
- 非 Ubuntu 20.04 系统环境依赖: 
- python3.7                     #    https://blog.csdn.net/hello_world_banni/article/details/108297775
- glibc-2.29                    #    https://blog.csdn.net/Nice_zou/article/details/116699692 
                                #    sudo rm /lib/x86_64-linux-gnu/libm.so.6 
                                #    sudo ln -s /usr/local/lib/libm-2.29.so /lib/x86_64-linux-gnu/libm.so.6
                                #    谨慎处理(否则系统可能崩溃) su root -> sudo rm /lib/x86_64-linux-gnu/libc.so.6
                                #    sudo ln -s /usr/local/lib/libc-2.29.so /lib/x86_64-linux-gnu/libc.so.6
- gcc9                          #    https://askubuntu.com/questions/1140183/install-gcc-9-on-ubuntu-18-04 
- g++9                          #    https://gist.github.com/alexandreelise/192a63e287018ddfc896bbcb70b219d4 

- 其他工具make, cmake, 编辑工具和软件依赖根据系统提示安装
```

### 参考文献和手册
1. RISC-V-Reader-Chinese-v2.3p1, 见 https://gitee.com/EasyRiscv/easy-riscv/tree/easy-riscv-v1.0/doc
2. verilator 手册, 见 https://gitee.com/EasyRiscv/easy-riscv/tree/easy-riscv-v1.0/doc
3. 夏宇闻编著. Verilog数字系统设计教程 第3版. 北京：北京航空航天大学出版社, 2017.08.
4. JohnL.HennessyDavidA.Patterson；贾洪峰译. 计算机体系结构 量化研究方法 第5版. 北京：人民邮电出版社, 2013.01.
5. git 常用命令见 https://git-scm.com/book/zh/v2
6. 鸟哥著；LINUX中国改变. 鸟哥的Linux私房菜 基础学习篇 第4版. 北京：人民邮电出版社, 2018.09.

### 目录结构
```
├── SourceMe                    #	环境变量设置
├── hw                          #	硬件代码目录
│   ├── rtl                     #	RTL 代码和 filelist
│   └── verify                  #	验证目录
│       ├── top.v               #	Verilog Testbench (包含 EASY_RISCV_TOP 例化)
│       ├── sim_main.cpp        #	Verilator Testbench (包含 Verilog Testbench 例化)
│       ├── Makefile            #	编译脚本 (支持 make cmp 编译, make run 运行, make wave 开波形)
│       ├── run.sh              #	回归脚本 (支持3个选项 simple-asm，simple-c，isa 分别运行对应测试)
│       ├── logs                #	临时目录包含 easy_riscv.vcd 波形
│       ├── testcase.dump       # 	临时文件当前用例反汇编
│       ├── inst.bin.str        #	临时文件当前用例指令段加载文件
│       ├── data.bin.str        #	临时文件当前用例数据段加载文件
│       └── run_results         #	临时目录包含用例 log
└── sw                          #	软件代码目录
    ├── riscv-tests             #	riscv-test 测试
    │   └── isa                 #	RV32I ISA 测试
    ├── scripts                 #	编译脚本
    │   ├── build.sh            #	主要编译脚本 (支持3个选项 simple-asm，simple-c，isa 分别编译对应目录)
    │   └── convert-sim.py      #	转换 bin to Memory 可加载 hex 格式
    ├── simple-asm              #	hello 汇编测试（须最先跑通）
    │   ├── build               #	汇编编译输出
    │   └── src                 #	汇编代码
    └── simple-c                #	hello world C 测试
        ├── build               #	C 编译输出
        └── src                 #	C 代码
```
- 详细 RTL 和 Verify 信息请参考 hw/README.md

### 运行流程
按照环境需求安装工具和软件依赖
1. fork 或 clone easy-riscv-v1.0 分支，主目录 source SourceMe
2. sw/script 目录运行 ./build.sh simple-asm
3. hw/verify 目录运行 ./run.sh simple-asm，默认只支持lui和addi指令，用例无法正常结束，其他指令须自行补全
4. make wave 查看波形，默认波形如下，前2条指令实现 0x1000+0x24=0x1024
![输入图片说明](https://images.gitee.com/uploads/images/2021/0809/190443_e5c13edc_8995020.png "Snipaste_2021-08-09_14-57-11.png")

5. simple-asm PASSED 后可按上述流程分别完成 simple-c (打印hello world)和 isa 测试, PASSED 打印内容请参考 hw/README.md

### 系统概述
  easy-riscv-v1.0总体结构框图如下图所示。分为3级流水，主要包含IFU、IDU、IEX、WB等主要模块。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0809/192653_4eb7e661_9535357.png "屏幕截图.png")
### 系统方案
1. 核心模块功能描述
- IFU功能说明

  完成取指阶段的功能，包括PC的控制以及从指令存储器中取指的过程。本模块在core的启动信号（i_start_up）启动下，开启PC取指，并从指令存储器读出对应PC的指令码。如果检查出PC是非对齐的，或上报异常。
- IDU功能说明

  完成译码阶段的功能。对取回的指令进行拆分和解释，识别区分不同指令类别以及获取各种操作数。IDU中除了译码功能，还包括Register File读写和异常的处理。 译码阶段如果解析出非法指令，会产生非法指令异常。同时也会处理其他模块发送过来的异常类型，包括PC非对齐和访存地址非对齐异常。如果产生异常并且该异常类型没有mask，会阻塞住IFU，不让其再取值。
- IEX功能说明

  完成指令所规定的各种操作，实现指令的具体功能。主要包含运算、跳转、访存指令的处理。
- WB功能说明

  将指令执行阶段的运行结果写回到register file。
2. 接口信号
- CORE接口信号说明

![输入图片说明](https://images.gitee.com/uploads/images/2021/0809/163227_e844cca9_9535357.png "屏幕截图.png")

- IFU接口信号说明

![输入图片说明](https://images.gitee.com/uploads/images/2021/0809/163213_7173a598_9535357.png "屏幕截图.png")

- IDU接口信号说明

![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/013544_5ce384ab_8995020.png "1.png")

- IEX接口信号说明

![输入图片说明](https://images.gitee.com/uploads/images/2021/0809/191947_b55a8bd7_9535357.png "屏幕截图.png")

- WB接口信号说明

![输入图片说明](https://images.gitee.com/uploads/images/2021/0809/192025_96c24d67_9535357.png "屏幕截图.png")


-  **_最后欢迎通过issue提供宝贵建议或通过git贡献代码_** 