//-------------------------------------- 
// easy-riscv core
//--------------------------------------

module EASY_RISCV_TOP (
    input  wire         i_clk
,   input  wire         i_rst_n
,   input  wire         i_start_up
,   input  wire [31:0]  i_start_pc
,   input  wire [2:0]   i_excp_mask_config
,   output wire [1:0]   o_core_status
,   output wire [2:0]   o_exceptions
,   output wire [31:0]  o_exceptions_pc
);

//--------------------------------------
wire            o_branch_vld_e0     ;
wire [31:0]     o_branch_pc_e0      ;
wire            o_fetch_stall_d0    ;
wire            o_instr_vld_f1      ;
wire [31:0]     o_dec_instr_f1      ;
wire [31:0]     o_cur_pc_f1         ;
wire            o_excp_pc_vld_f1    ;
wire [4:0]      o_rf_rd_idx_c0      ;
wire            o_rf_rd_vld_c0      ;
wire [31:0]     o_rf_rd_dat_c0      ;
wire            o_instr_vld_d0      ;
wire [31:0]     o_cur_pc_d0         ;
wire [5:0]      o_what_is_instr_d0  ;
wire [4:0]      o_rd_index_d0       ;
wire [31:0]     o_rs1_data_d0       ;
wire [31:0]     o_rs2_data_d0       ;
wire [4:0]      o_shamt_d0          ;
wire [31:0]     o_imm_data_d0       ;
wire [5:0]      o_what_is_instr_e1  ;
wire            o_unaligned_access_fault_e0;
wire [3:0]      o_rf_rd_be_e1       ;
wire [4:0]      o_rf_rd_idx_e1      ;
wire [31:0]     o_rf_rd_dat_e1      ;
wire [4:0]      o_rf_rd_idx_e0      ;
wire            o_rf_rd_vld_e0      ;
wire [31:0]     o_rf_rd_dat_e0      ;

//--------------------------------------
EASY_RISCV_IFU U_EASY_RISCV_IFU(
    .i_clk                      (i_clk                      )
,   .i_rst_n                    (i_rst_n                    )
,   .i_start_up                 (i_start_up                 )
,   .i_start_pc                 (i_start_pc                 )
,   .i_jump_instr_vld_e0        (o_branch_vld_e0            )
,   .i_jump_instr_pc_e0         (o_branch_pc_e0             )
,   .i_fetch_stall_d0           (o_fetch_stall_d0           )
,   .o_instr_vld_f1             (o_instr_vld_f1             )
,   .o_dec_instr_f1             (o_dec_instr_f1             )
,   .o_cur_pc_f1                (o_cur_pc_f1                )
,   .o_excp_pc_vld_f1           (o_excp_pc_vld_f1           )
,   .o_core_status              (o_core_status              )
);

EASY_RISCV_IDU U_EASY_RISCV_IDU(
    .i_clk                      (i_clk                      )
,   .i_rst_n                    (i_rst_n                    )
,   .i_instr_vld_f1             (o_instr_vld_f1             )
,   .i_dec_instr_f1             (o_dec_instr_f1             )
,   .i_cur_pc_f1                (o_cur_pc_f1                )
,   .i_excp_pc_vld_f1           (o_excp_pc_vld_f1           )
,   .i_excp_unaligned_access_e0 (1'b0                       )
,   .i_excp_mask_config         (i_excp_mask_config         )
,   .i_rf_wr_vld_c0             (o_rf_rd_vld_c0             ) 
,   .i_rf_wr_index_c0           (o_rf_rd_idx_c0             )
,   .i_rf_wr_data_c0            (o_rf_rd_dat_c0             )
,   .i_rf_wr_index_e0           (o_rf_rd_idx_e0             )
,   .i_rf_wr_vld_e0             (o_rf_rd_vld_e0             )
,   .i_rf_wr_data_e0            (o_rf_rd_dat_e0             )
,   .o_instr_vld_d0             (o_instr_vld_d0             )
,   .o_cur_pc_d0                (o_cur_pc_d0                )
,   .o_what_is_instr_d0         (o_what_is_instr_d0         )
,   .o_rd_index_d0              (o_rd_index_d0              )
,   .o_rs1_data_d0              (o_rs1_data_d0              )
,   .o_rs2_data_d0              (o_rs2_data_d0              )
,   .o_shamt_d0                 (o_shamt_d0                 )
,   .o_imm_data_d0              (o_imm_data_d0              )
,   .o_excps_d1                 (o_exceptions               )
,   .o_excps_pc_d1              (o_exceptions_pc            )
,   .o_fetch_stall_d0           (o_fetch_stall_d0           )
);

EASY_RISCV_IEX_LSU U_EASY_RISCV_IEX_LSU(
    .i_clk                      (i_clk                      )
,   .i_rst_n                    (i_rst_n                    )
,   .i_instr_vld_d0             (o_instr_vld_d0             )
,   .i_what_is_instr_d0         (o_what_is_instr_d0         )
,   .i_cur_pc_d0                (o_cur_pc_d0                )
,   .i_shamt_d0                 (o_shamt_d0                 )
,   .i_imm_dat_d0               (o_imm_data_d0              )
,   .i_rf_r1_dat_d0             (o_rs1_data_d0              )
,   .i_rf_r2_dat_d0             (o_rs2_data_d0              )
,   .i_rf_rd_idx_d0             (o_rd_index_d0              )
,   .o_branch_vld_e0            (o_branch_vld_e0            )
,   .o_branch_pc_e0             (o_branch_pc_e0             )
,   .o_unaligned_access_fault_e0(o_unaligned_access_fault_e0)
,   .o_what_is_instr_e1         (o_what_is_instr_e1         )
,   .o_rf_rd_be_e1              (o_rf_rd_be_e1              )
,   .o_rf_rd_idx_e1             (o_rf_rd_idx_e1             )
,   .o_rf_rd_dat_e1             (o_rf_rd_dat_e1             )
,   .o_rf_rd_idx_e0             (o_rf_rd_idx_e0             )
,   .o_rf_rd_vld_e0             (o_rf_rd_vld_e0             )
,   .o_rf_rd_dat_e0             (o_rf_rd_dat_e0             )
);

EASY_RISCV_WB U_EASY_RISCV_WB(
    .i_clk                      (i_clk                      )
,   .i_rst_n                    (i_rst_n                    )
,   .i_what_is_instr_e1         (o_what_is_instr_e1         )
,   .i_rf_rd_be_e1              (o_rf_rd_be_e1              )
,   .i_rf_rd_idx_e1             (o_rf_rd_idx_e1             )
,   .i_rf_rd_dat_e1             (o_rf_rd_dat_e1             )
,   .o_rf_rd_idx_c0             (o_rf_rd_idx_c0             )
,   .o_rf_rd_vld_c0             (o_rf_rd_vld_c0             )
,   .o_rf_rd_dat_c0             (o_rf_rd_dat_c0             )
);

//--------------------------------------
endmodule

