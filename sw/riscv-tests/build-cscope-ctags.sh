#!/bin/sh

find . -name "*.h" -o -name "*.S" -o -name "*.s" -o -name "*.c" -o -name "*.cc"  > cscope.files
cscope -bkq -i cscope.files
ctags -R
