# easy-riscv

#### 介绍
本环境主要用于开发riscv虚拟项目中CPU运行的软件，环境提供bare-metal C语言开发和简单汇编指令开发两种

#### 软件架构
- simple-c：bare-metal软件框架。根据riscv硬件开发简单C语言程序的框架。
- simple-asm：简单汇编指令示意。
- riscv-tests：riscv 指令测试集，目前编译输出仅支持rv32i。
- scripts：配置信息和编译脚本。

#### 安装教程

1.  下载源码
- git clone https://gitee.com/EasyRiscv/easy-riscv.git
2.  下载Riscv交叉编译工具
- 因为[riscv交叉编译工具](https://github.com/riscv/riscv-gnu-toolchain)编译比较耗时，且通常需要处理一些环境依赖。我们将预编译好的可执行文件打包上传到了独立仓库，请使用以下指令下载
- git clone https://gitee.com/EasyRiscv/riscv-gnu-tool.git
- cat gcc-riscv.tar.gz.* > gcc-riscv.tar.gz
- tar xvf gcc-riscv.tar.gz
3.  设置环境变量
- sudo vim /etc/profile
- 在/etc/profile中添加：
- export PATH={your path}/gcc-riscv/bin:$PATH
- 然后关闭文件后source /etc/profile

![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/113520_fcc0c337_8018479.png "path.png")

4.  安装GCC/Python3/cmake

- ubuntu20.4默认安装python3.8，软件要求python3版本为3.6及以上
- ubuntu20.4默认未安装GCC,可以通过如下命令安装
- sudo apt update
- sudo apt install build-essential
- ubuntu20.4默认未安装cmake，可以通过如下命令安装
- sudo apt install cmake


#### 使用说明

所有依赖工具安装好后，进入到scripts目录开始编译用例
1.  执行./build simple-asm
2.  执行./build simple-c
3.  执行./build isa

#### 程序地址空间说明
- riscv的配置为：2KB代码空间，4KB数据空间，软件认为的地址空间视图
    - Rom：0x00000000-0x000007FF
    - Ram：0x00000800-0x000017FF
- 需要注意的是，riscv设计没有统一的地址空间（因为这两段空间在硬件上是独立两个memory空间，没有总线），而编译器在链接阶段不允许有地址重叠的两段memory（也就是ld不允许链接文件定义两段有地址重叠的memory），所以riscv的LSU要对地址访问做特殊处理：所有访问数据空间相关的地址要减去0x800（相当于模拟统一地址空间，具体内容参考lsu的参考设计）


#### 测试程序与仿真环境的交互机制

测试程序通过DMEM区域的最后16byte空间来与仿真环境通信，我们事先定义好各个地址的功能，仿真环境在运行时不断地polling这些地址的写操作，并作出相应的动作。具体的使用情况如下：
|  程序地址  | DMEM地址   | 含义   |  相关测试用例 |
|  ----   | :----   |  :----  | :----  |
| 0x17F0   | 0xFF0   | 作为系统打印的地址，程序不断地向该地址写入需要打印的字符，仿真环境将转而输出到终端  |  simple-asm/simple-c |
| 0x17F1   | 0xFF1   | 作为结束仿真的条件，一个测试程序在结束时向该地址写1  | isa/simple-asm/simple-c  |
| 0x17F2   | 0xFF2   | 指示单指令测试的结果1：success 2：fail | isa  |
| 0x17F3   | 0xFF3   |  如果单指令测试fail，该地址表示测试用例fail的具体位置(注：单指令测试中包含不定数据的独立case，这里是指fail时的case num，方便用户根据反汇编信息快速定位到fail的具体位置) | isa  |


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

