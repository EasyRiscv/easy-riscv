#!/bin/sh
show_usage()
{
    echo "
    ./run.sh <target>
    target:
        isa:        run rv32ui isa test cases
        simple-asm: run simple-asm test case
        simple-c:   run simple-c test case
    "
    exit
}

rm -rf run_results
mkdir -p run_results
run_dir=`pwd`
run_results_dir=./run_results
if [ $# -eq 1 ]; then
    if [[ "$1" == "isa" ]]; then
        bin_dir="${EASY_RISCV_SW_ROOT}/riscv-tests/isa"
    elif [[ "$1" == "simple-asm" ]]; then
        bin_dir="${EASY_RISCV_SW_ROOT}/simple-asm/build"
    elif [[ "$1" == "simple-c" ]]; then       
        bin_dir="${EASY_RISCV_SW_ROOT}/simple-c/build"
    else
        show_usage
    fi
else
    show_usage
fi

# check dirs
if [ ! -d "$bin_dir" ]; then
    echo "$bin_dir do not exist, please check env variable: EASY_RISCV_MEM_WRAP_ROOT or rebuild your tests"
    exit
fi

if [ ! -d "$run_dir" ]; then
    echo "$run_dir do not exist, please check env variable: EASY_RISCV_MEM_WRAP_ROOT"
    exit
fi

data_dst="${run_dir}/data.bin.str"
inst_dst="${run_dir}/inst.bin.str"
dump_dst="${run_dir}/testcase.dump"

run_log=$run_results_dir/results.log

bin_names=`ls ${bin_dir}/*.bin.inst.str | awk -F "/" '{print $NF}' | awk -F "." '{print $1}'`
echo "*********************************************************"
echo "bin name is $bin_names"
echo "bin_dir  is $bin_dir"
echo "run_dir  is $run_dir"
echo "*********************************************************"
cnt_success=0
cnt_fail=0

# compile before batch test
make cmp

echo "test begin:" >> $run_log
echo "*********************************************************" >> $run_log

for bin in $bin_names
do
    echo "test case: $bin" >> $run_log
    echo "test case: $bin"
    data_src="${bin_dir}/${bin}.bin.data.str"
    inst_src="${bin_dir}/${bin}.bin.inst.str"
    dump_src="${bin_dir}/${bin}.dump"
    cp $data_src $data_dst
    if [ $? -ne 0 ]; then
        echo "check your data files"
        exit
    fi
    cp $inst_src $inst_dst
    if [ $? -ne 0 ]; then
        echo "check your instr files"
        exit
    fi
    cp $dump_src $dump_dst
    if [ $? -ne 0 ]; then
        echo "check your dump files"
        exit
    fi
## 
##    if [[ "$1" == "isa" ]]; then
    make run | tee ${run_results_dir}/runisa.log
##    else
##        make run
##    fi
## 
    result=`grep -ns 'TEST ' ${run_results_dir}/runisa.log`
    echo $result >> $run_log
    if [[ $result == *"FAILED"* ]]; then
        echo "*********************************************************"
        echo -e "\033[31m FAILED ! \033[0m"
        cnt_fail=$[cnt_fail+1]
        # save failed wave
        # if you want exit immediately after one case is failed, uncommon this
        mv ${run_results_dir}/runisa.log $run_results_dir/${bin}.log
        break
        # exit
    elif [[ $result == *"PASSED"* ]]; then
        echo "*********************************************************"
        echo -e "\033[32m PASSED ! \033[0m"
        cnt_success=$[cnt_success+1]
    else
 	    mv ${run_results_dir}/runisa.log $run_results_dir/${bin}.log
 	    echo "unknown error, I did NOT detect dmem write, you RTL may have load/store bug!"
 	    break
        # exit
    fi
    # remove tc_sanity*.log out of ./base_fun.log, as next case will 'grep' this directory
    # mv ${run_results_dir}/runisa.log $run_results_dir/${bin}.log
    rm -rf $data_dst $inst_dst $dump_dst
    echo "*********************************************************"
    echo "*********************************************************" >> $run_log
done
echo "test end" >> $run_log
echo "PASSED: $cnt_success FAILED: $cnt_fail" >> $run_log
echo "test end"
echo "PASSED: $cnt_success FAILED: $cnt_fail"


