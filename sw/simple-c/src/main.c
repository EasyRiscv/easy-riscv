
#include <stdint.h>

#define START  "\n--------------------user log start----------------------\n\n"
#define HELLO  "hello, world!\nhello, riscv!\n"
#define END  "\n--------------------user log end----------------------\n\n"

int test = 12345;
int test1;

#define LOG_BUFF_BASE 0x17F0

static uint32_t counter = 0;

// we use writing 1 to 0x13f1 as flag of simulation end
void end_simulation(void)
{
    *(volatile uint8_t *)(LOG_BUFF_BASE + 2) = 0x1; //sim success
    *(volatile uint8_t *)(LOG_BUFF_BASE + 1) = 0x1; //sim end
}

void write_str(char *ptr, int len)
{
    int i;
    for (i = 0; i < len; i++)
    {
        *(volatile uint8_t *)(LOG_BUFF_BASE) = *ptr++;
    }
}

int main(void)
{
    int a,b;
    a = 0x123456;
    b = 0;

    write_str(START, sizeof(START)-1);
    write_str(HELLO, sizeof(HELLO)-1);

    test1 = test;
    test = a + b;

    write_str(END, sizeof(END)-1);
    end_simulation();
    while(1);
    return 0;
}
